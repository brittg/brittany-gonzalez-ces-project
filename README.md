# Brittany Gonzalez CES Project



# A Guide to Visiting Yosemite National Park

Visting Yosemite National Park is a must-do for any national parks aficionado. From stunning waterfalls to picturesque lakes to quiet meadows and soaring cliffs, there's something for every nature lover at this magnificent park. What follows is a collection of photos of the park, and a guide to planning your visit, including park highlights, handy tips, and important events and dates. Enjoy your trip!

Already a Yosemite pro? Skip down to [make your reservation](#make-your-reservation)!

## Review the latest park updates

- [ ] Visit the Park's website at [Yosemite National Park](https://www.nps.gov/yose/planyourvisit/faq.htm). Regardless of the time of year you visit, Yosemite National Park is subject to closures, alerts, and other changes. For example, currently Tioga Road Highway and Glacier Point Road are closed for the season due to snow. Tioga Road usually opens in late May or June, but check the site to verify. Other restrictions may also be in place due to Covid-19.

## Choose your must-see places

- [ ] [Lakes and Rivers](https://www.nps.gov/yose/planyourvisit/lakes.htm) Most lakes in Yosemite are in the wilderness and require hiking. Tenaya Lake is one of the easiest to get to when open, and is quite popular for picnicking, swimming, and canoeing. Rivers flow through Wawona, Yosemite Valley, and Tuolumne Meadows, and numerous seasonal creeks flow throughout the park. Take a look at a park map for details.
- [ ] [Yosemite Valley](https://www.nps.gov/yose/planyourvisit/yv.htm) Get an up-close view of the gorgeous valley floor. Don't miss it!

## Lodging & Camping

When visiting Yosemite National Park, you're welcome to simply drive through by simply paying an entrance fee and stay the day. If you wish the spend the night, you will need a camping or lodging reservation. Accommodations range from basic motels just outside the park to the luxurious Awahnee Hotel. Feel free to pitch your tent in the wilderness - don't forget a wilderness permit! - or go for a glamping option at Curry Village or Tuolomne Meadows. 

- [ ] [The Awahnee Hotel](https://www.travelyosemite.com/lodging/the-ahwahnee/)
- [ ] [Curry Village](https://www.travelyosemite.com/lodging/curry-village)
- [ ] [Tuolomne Meadows](https://www.travelyosemite.com/lodging/tuolumne-meadows-lodge)

## Park Maps

Use these maps to help find your way to and around the park.

- [ ] [Yosemite National Park Map](https://www.nps.gov/yose/planyourvisit/maps.htm)
- [ ] [California State Map](https://www.nps.gov/state/ca/index.htm?program=parks)
- [ ] [Topographic Trail Maps](https://www.nps.gov/yose/planyourvisit/trails.htm)

## Park Hazards

While Yosemite National Park is a natural paradise, it also has its share of hazards to be aware of. Make sure you're prepared to encounter mosquitoes, poison oak, inclement weather such as lightening and heavy rains, wildfires, and last but not least, the occasional bear.

## Make Your Reservation

All set to start your adventure? Visit the Park's [reservations page](https://www.nps.gov/yose/planyourvisit/permitsandreservations.htm) to get overnight permits and buy your entrance fee. Note that you may keep your entrance fee receipt to come and go through the park for up to a week, so don't lose it!

# Authors and acknowledgment

Author is Brittany Gonzalez. She may be reached at bgonzalez629@gmail.com. Many thanks to the National Parks Service and National Forest Service for preserving and maintaining this remarkable park. Also thank you to the author of this README for producing and sharing the visual guide to Yosemite National Park. 

# License

Licensed through GitLab.

# Project status

This project is currently paused, but fellow Yosemite fans are free to edit this guide and share their photos for the wider parks community.
